# -*- coding: utf-8 -*-

#from gluon.serializers import json
import gluon.contrib.simplejson
import json
import datetime
import time


def index():
    rows = db(db.air_conditions.temperature != "-999.0").select()
    c_data = {}
    temperature = []    # x: time, y: temp
    humidity = []

    i = 0
    for r in rows:
        c_data[i] = {
            "temperature": r.temperature * 3.38,
            "humidity": r.humidity,
            "created_on": r.created_on
        }
        temperature.append({'created_on': time.mktime(r.created_on.timetuple()), 'temperature': r.temperature * 3.38})
        humidity.append({'created_on': time.mktime(r.created_on.timetuple()), 'humidity': r.humidity})

        i += 1

    return dict(
        data=c_data,
        temperature=temperature,
        humidity=humidity
    )


def listen():
    try:
        t_data = gluon.contrib.simplejson.loads(request.body.read())

        if t_data:
            raw_data = json.dumps(t_data)
            db.air_conditions.insert(
                raw_data=raw_data,
                temperature=t_data["temperature"],
                humidity=t_data["humidity"]
            )
        else:
            pass
    except:
        return None


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_signature()
def data():
    """
    http://..../[app]/default/data/tables
    http://..../[app]/default/data/create/[table]
    http://..../[app]/default/data/read/[table]/[id]
    http://..../[app]/default/data/update/[table]/[id]
    http://..../[app]/default/data/delete/[table]/[id]
    http://..../[app]/default/data/select/[table]
    http://..../[app]/default/data/search/[table]
    but URLs must be signed, i.e. linked with
      A('table',_href=URL('data/tables',user_signature=True))
    or with the signed load operator
      LOAD('default','data.load',args='tables',ajax=True,user_signature=True)
    """
    return dict(form=crud())
